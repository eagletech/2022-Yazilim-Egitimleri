# kullanıcıdan sayı ve işlemi almalısınız - girdi (input) birimi
# işlem birimi
# gösterim

def factorial(num1):
    res = 1;
    for i in range(2, num1 + 1):
        res *= i

    return res

def main():
    while True:
        num1 = float(input("Sayı 1: "))
        num2 = float(input("Sayı 2: "))

        """
        bool()
        str()
        """

        print("""
            [1] toplama 
            [2] çıkarma
            [3] çarpma
            [4] bölme
              \n""")
        opt = input("İşlem: ")

        """
        <
        >
        <=
        >= 
        !=
        ==
        """
        if opt == "1":
            res = num1 + num2;
        elif opt == "2":
            res = num1 - num2;
        elif opt == "3":
            res = num1 * num2;
        elif opt == "4":
            res = num1 / num2;
        else:
            print("Yanlış işlem seçimi.");

        print(res);

# main();
print(factorial(6));
