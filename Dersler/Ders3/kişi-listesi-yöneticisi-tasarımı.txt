menü/işlev
	kişileri görüntüle
	kişi ekle
	kişi sil
	kişi düzenle
	yardım
	çıkış

başlangıç 
	kişi listesi yüklenir 

kişi görüntüleme 
	pydoc/pager

kişi ekleme
	bilgiler sırayla alınır, numaranın sayılardan oluştuğundan emin olunur ve liste ile dosyaya kaydedilir

kişi silme
	kişi arama sonuçlarından (ekranda print ile gösterilip numaralandırılır ve kullanıcının numara seçmesi istenir) kullanıcının seçtiği ögenin listeden ve dosyasının silinmesi. arama esnasında çıkan sonuçların listedeki indeksleri ve ID'leri yalnızca depolanır

kişi düzenleme
	kişi arama sonuçlarından (ekranda print ile gösterilip numaralandırılır ve kullanıcının numara seçmesi istenir) kullanıcının seçtiği ögenin liste ve dosyadaki kopyasının üstüne yeni bilgilerin yazılması. arama esnasında çıkan sonuçların listedeki indeksleri ve ID'leri yalnızca depolanır

kişi arama
	str tipinde 'in' kullanımı

depolama
	kişilerin isimleri, soyisimleri, telefon numaraları, ID'leri bir dict olarak list'de listelenir.
	her biri için kişiler klasöründe uzantısız bir dosya (dosyanın adı ilgili kişinin IDsi ve ID = kişi eklendiği anda var olan son kişinin ID'si + 1) 

dosya sentaksı
	her satıra bir bilgi (isim, soyisim, telefon no)
