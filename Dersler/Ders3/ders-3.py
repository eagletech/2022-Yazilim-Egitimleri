max_id = -1
contacts = []

ROOT_F = ".\contacts" # Mac'de '\' yerine '/' konulmalı.

def init():
    global max_id

    try: 
        for i in os.listdir(ROOT_F):
            with open(f"{ROOT_F}\{i}", "r") as f: # Mac'de '\' yerine '/' konulmalı.
                name, sname, phone = f.readlines();
                contacts.append(
                        {
                        "name" : name.strip(), 
                        "surname" : sname.strip(), 
                        "phone" : phone.strip(), 
                        "ID" : int(i)
                        }
                );

                if int(i) > max_id:
                    max_id = int(i)

    except ValueError:
        print("Bozulmuş veritabanı dosyaları...")
        exit(1)

    except FileNotFoundError:
        os.mkdir(ROOT_F);

def get_contact_info():

    while True:
        name = input("İsim: ")
        sname = input("Soyisim: ")
        phone = input("Tel No: ")
        
        if not name or not sname or not phone:
            print("Lütfen tüm alanları doldurun!");
            continue

        break;

    return {"name" : name, "surname" : sname, "phone" : phone};

def view_contacts(): # kişileri görüntüle
    pass

def add_contact():
    global max_id

    contact = get_contact_info();

    ID = max_id + 1;
    max_id = ID;

    contact["id"] = ID;

    # Mac'de '\' yerine '/' konulmalı.
    with open(f"{ROOT_F}\{ID}", "w") as f: # .\contacts\1
        f.write(contact["name"] + "\n")
        f.write(contact["surname"] + "\n")
        f.write(contact["phone"] + "\n")

    contacts.append(contact);

    print("Başarıyla kaydedildi!\n");

def del_contact():
    pass

def edit_contact():
    pass
def main():
    
    # init()

        while True:
            try:
                opt = int(input("""
                [1] Kişileri Görüntüle
                [2] Kişi Ekle
                [3] Kişi Sil
                [4] Kişi Düzenle
                [5] Çıkış

                İşlem: """));
            except:
                print("\nLütfen belirtilen işlemlerden birini girin.");
                continue;

            print()

            if opt == 1:
                view_contacts();

            elif opt == 2:
                add_contact();

            elif opt == 3:
                del_contact();

            elif opt == 4:
                edit_contact();

            elif opt == 5:
                exit(0);

            else:
                print("Geçersiz işlem, lütfen tekrar deneyin.")

if __name__ == "__main__":
    main();
