SDLC, sistem-yazılım geliştirme alanlarının ortak dalıdır. En az kaynak (zaman, para, iş gücü vs) kullanarak bir yazılımın veya sistemin geliştirilmesini amaçlayan bir plandır.
SDLC'nin farklı versiyonları bulunsa da genel olarak adımlar aynıdır. Çoğunlukla değişen yöntem olur. Bu yöntemlere örnek olarak Waterfall, B, V Modelleri gösterilebilir.
Aşamalar şu şekildedir:
Problem belirlenir. (Mesela: İnsanlar için çevrimiçi bir mesajlaşma platformu bulunmamaktadır.)
Bu amaçlar için gereken gereksinimler en kapsamlı olanlarından en detaylara kadar belirlenir. (Mesela: İnsanlar hesaplarına giriş yapıp mesajlaşmak istedikleri kişileri seçebilmeliler.)
Gereksinimler analiz edilir. (Mesela: Giriş yapmak için bizim e-mail ve şifre yazmak için birer bölüme ve giriş butonuna ihtiyacımız var. Bunlardan giriş alındıktan sonra veriler DB'den kontrol edilerek hesaba giriş işlemi onaylanır veya reddedilir.)
Dizayn. (Mesela: E-mail ve şifre alanları alt alta olsun. Bunların uygulanması için React framework'unu kullanalım.)
Geliştirme. (Mesela: Kod yazılır.)
Test. (Geri bildirimli bir hata veya kullanışsızlık veya mantıksızlık bulma döngüsüdür.)
Yayınlama. (Marketing Departmanları işe dahil olur.)
