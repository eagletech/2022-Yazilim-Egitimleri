import pydoc
import os


ROOT_F = "./contacts"


contacts = []
max_id = 0


def init():
    global max_id

    try: 
        for i in os.listdir(ROOT_F):
            with open(f"{ROOT_F}/{i}", "r") as f:
                name, sname, phone = f.readlines();
                contacts.append(
                        {
                        "name" : name.strip(), 
                        "surname" : sname.strip(), 
                        "phone" : phone.strip(), 
                        "id" : int(i)
                        }
                );

                if int(i) > max_id:
                    max_id = int(i)

    except ValueError:
        print("Bozulmuş veritabanı dosyaları...")
        exit(1)

    except FileNotFoundError:
        os.mkdir(ROOT_F);
             

def get_contact_info():

    while True:
        name = input("İsim: ")
        sname = input("Soyisim: ")
        phone = input("Tel No: ")
        
        if not name or not sname or not phone:
            print("Lütfen tüm alanları doldurun!");
            continue

        break;

    return {"name" : name, "surname" : sname, "phone" : phone};

def search_contact():
    pattern = input("İsim/Soyisim/Tel. No. (çıkmak için boş bırakın): ");

    match = []

    for i in contacts:
        if pattern.lower() in i["name"] + i["surname"] or pattern.lower() in i["phone"]:
            match += [i];

    return match

def select_contact(matches : list):
    """
return value:
    -1 : No selection
    """

    if len(matches) < 1:
        return -1;

    for i in range(len(matches)):
        print(f"{i + 1}. Kişi:\t{matches[i]['name']} {matches[i]['surname']}")

    while True:
        try:
            sel = int(input("Lütfen seçiminizi girin (çıkmak için q): "))

        except: 
            if sel == "q":
                return -1;

            print("Lütfen geçerli bir değer girin.")

        else:
            if not 1 <= sel <= len(matches):
                print("Lütfen geçerli bir değer girin.")
                continue;

            return sel - 1;

def view_contacts(contact_list : list):
    text = ""
    for i in contact_list:
        text += "----------------------------\n"
        text += "İsim: " + i["name"] + "\n"
        text += "Soyisim: " + i["surname"] + "\n"
        text += "Tel. No.: " + i["phone"] + "\n"

        print(text)

        text = str()


def edit_contact():
    index = select_contact(search_contact());
    
    if index == -1: return;

    print("\nKişinin yeni bilgilerini girin:")
    new = get_contact_info();


    ID = contacts[index]["id"]

    contacts[index] = new
    contacts[index]["id"] = ID

    with open(f"{ROOT_F}/{ID}", "w") as f:
        f.write(new["name"] + "\n")
        f.write(new["surname"] + "\n")
        f.write(new["phone"] + "\n")

    print("\nKişi başarıyla düzenlendi.");


def del_contact():
    index = select_contact(search_contact());

    if index == -1: return;

    ID = contacts[index]["id"]

    os.remove(f"{ROOT_F}/{ID}");
    del contacts[index]

    print("\nKişi başarıyla silindi.");

def add_contact():
    global max_id

    contact = get_contact_info();

    ID = max_id + 1;
    max_id = ID;

    contact["id"] = ID;

    with open(f"{ROOT_F}/{ID}", "w") as f:
        f.write(contact["name"] + "\n")
        f.write(contact["surname"] + "\n")
        f.write(contact["phone"] + "\n")

    contacts.append(contact);

    print("Başarıyla kaydedildi!\n");


def main():
    
    init()

    while True:
        try:
            opt = int(input("""
    [1] Kişileri Görüntüle
    [2] Kişi Ekle
    [3] Kişi Sil
    [4] Kişi Düzenle
    [5] Çıkış

    İşlem: """));

        except:
            print("\nLütfen belirtilen işlemlerden birini girin.");
            continue;

        print()

        if opt == 1:
            view_contacts(contacts);

        elif opt == 2:
            add_contact();

        elif opt == 3:
            del_contact();

        elif opt == 4:
            edit_contact();

        elif opt == 5:
            exit(0);

        else:
            print("Geçersiz işlem, lütfen tekrar deneyin.")

if __name__ == "__main__":
    main();
